-- XMonad Config --

-- Imports --
 -- Base
import XMonad
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

 -- Actions
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WithAll (sinkAll, killAll)

 -- Data
import Data.Maybe (fromJust)
import Data.Monoid
import qualified Data.Map as M

 -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks (avoidStruts, docks, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat, doHideIgnore)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WindowSwallowing

 -- Layouts
import XMonad.Layout.SimplestFloat
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed

 -- Layout modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.NoBorders
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

 -- Utils
import XMonad.Util.EZConfig (additionalKeysP)
import qualified XMonad.Util.Hacks as Hacks
import XMonad.Util.Loggers --(logDefault, logTitle)
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe, hPutStrLn)
import XMonad.Util.SpawnOnce

-- Variables --
myFont :: String
myFont = "xft:Font Awesome Free Solid:pixelsize=20"

myModMask :: KeyMask
myModMask = mod4Mask

myTerminal :: String
myTerminal = "alacritty"

myEmacs :: String
myEmacs = "emacsclient -c -a 'emacs'"

myEditor :: String
myEditor = "emacsclient -c -a 'emacs'"

myFlex :: String
myFlex = "yad ~/.xmonad/scripts/flex.sh"

myBrowser :: String
myBrowser = "librewolf"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth :: Dimension
myBorderWidth = 1

myNormColor :: String
myNormColor = "#32344a"

myFocusColor :: String
myFocusColor = "#7da6ff"

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- Startup --
myStartupHook :: X ()
myStartupHook = do
        spawn "killall trayer"
        spawn "killall conky"

        --spawnOnce "nitrogen --set-zoom-fill --random &"
        spawnOnce "xsetroot -cursor_name left_ptr"
	spawnOnce "nitrogen --restore &"
        spawnOnce "lxsession"
        spawnOnce "picom"
        spawnOnce "nm-applet"
        spawnOnce "volumeicon"

        spawn "/usr/bin/emacs --daemon"
        spawn "sleep 15 && trayer -l --edge top --align right --widthtype request --padding 3 --SetDockType true --SetPartialStrut true --expand true --height 25"
        spawn "sleep 5 && conky -c $HOME/.config/conky/weather/tokyo-night-title.conkyrc"
        spawn "sleep 5 && conky -c $HOME/.config/conky/weather/tokyo-night-main.conkyrc"

        setWMName "LG3D"

-- Layouts --

 -- Spacing
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

 -- Layouts
tall    = renamed [Replace "tall"]
          $ smartBorders
          $ windowNavigation
          $ addTabs shrinkText myTabTheme
          $ subLayout [] (smartBorders Simplest)
          $ limitWindows 12
          $ mySpacing 8
          $ ResizableTall 1 (3/100) (1/2) []
monocle = renamed [Replace "monocle"]
          $ smartBorders
          $ windowNavigation
          $ addTabs shrinkText myTabTheme
          $ subLayout [] (smartBorders Simplest)
          $ limitWindows 20 Full
floats  = renamed [Replace "floats"]
          $ smartBorders
          $ limitWindows 20 simplestFloat
tabs    = renamed [Replace "tabs"]
          $ tabbed shrinkText myTabTheme

myTabTheme = def { fontName        = myFont
             , activeColor         = "#FFFFFF"
             , inactiveColor       = "#3b405c"
             , activeBorderColor   = "#685794"
             , inactiveBorderColor = "#24283b"
             , activeTextColor     = "#24283b"
             , inactiveTextColor   = "#a9b1d6"
                 }


 -- Layout hook
myLayout = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth tall
                                 ||| noBorders monocle
                                 ||| floats
                                 ||| noBorders tabs
                                 
-- Workspaces --
myWorkspaces = ["web","chat","games","launchers","work","sfx","gfx","misc","misc-2"]

 -- Prints current workspace when changing workspaces
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:NotoSans:style=bold:size=60:antialias=true:hinting=true"
    , swn_fade              = 1.2
    , swn_bgcolor           = "#1a1b26"
    , swn_color             = "#a9b1d6"
    }

-- Manage hook --
myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
    [  -- Dialogue, notifications, etc.
      className =? "confirm"         --> doFloat
    , className =? "dialog"          --> doFloat
    , className =? "download"        --> doFloat
    , className =? "error"           --> doFloat
    , className =? "file_progress"   --> doFloat
    , className =? "notification"    --> doFloat
    , className =? "splash"          --> doFloat
    , className =? "toolbar"         --> doFloat
    , className =? "Yad"             --> doCenterFloat

       -- Apps
    , className =? "Brave-browser"   --> doShift ( myWorkspaces !! 0 )
    , className =? "LibreWolf"       --> doShift ( myWorkspaces !! 0 )

    , className =? "Mailspring"      --> doShift ( myWorkspaces !! 7 )

    , className =? "Gimp"            --> doShift ( myWorkspaces !! 6 )
    , className =? "Gimp"            --> doFloat

    , className =? "Pixelorama"      --> doShift ( myWorkspaces !! 6 )
    , className =? "Pixelorama"      --> doFloat

    , className =? "nuclear"         --> doShift ( myWorkspaces !! 5 )
    , className =? "nuclear"         --> doCenterFloat

    , className =? "discord"         --> doShift ( myWorkspaces !! 1 )
    , className =? "Element"         --> doShift ( myWorkspaces !! 1 )

    , className =? "steam"           --> doShift ( myWorkspaces !! 3 )
    , className =? "steam"           --> doFloat

       -- Other
    , isFullscreen                   --> doFullFloat
     ]

-- Key bindings --
myKeys :: [(String, X ())]
myKeys =
    [  -- Xmonad
       ("M-S-r",   spawn "xmonad --restart")
     , ("M-r",     spawn "xmonad --recompile")
     , ("M-S-q",   spawn "logout-script")
     , ("M-<Esc>",   spawn "keymapswap")
       -- Dmenu
     , ("M-p", spawn "dmenu_run -z 1888 -x 16 -i -F -h 25 -g 7 -l 7 -p \"\61728 :\"")

       -- Dmenu scripts
     , ("M-S-p s", spawn "find-org-stories")
     , ("M-S-p k", spawn "proc-killer")

       -- Emacs
     , ("M-e e", spawn myEmacs)
     , ("M-e i", spawn "emacsclient --eval \"(emacs-everywhere)\"")
       -- Apps
     , ("M-<Return>",   spawn  myTerminal)
     , ("M-<KP_Enter>", spawn  myTerminal)
     , ("M-S-b",        spawn  myBrowser)

       -- Killing windows
     , ("M-S-c", kill)
     , ("M-S-a", killAll)

       -- Floating
     , ("M-f",   sendMessage (T.Toggle "floats")) -- toggles floating
     , ("M-t",   withFocused $ windows . W.sink)  -- push floating window into tiling
     , ("M-S-t", sinkAll)                         -- push all floating windows to tile

       -- Spacing
     , ("C-M1-h", decScreenSpacing 4) -- decrease screen spacing
     , ("C-M1-j", decWindowSpacing 4) -- decrease window spacing
     , ("C-M1-k", incWindowSpacing 4) -- increase window spacing
     , ("C-M1-l", incScreenSpacing 4) -- increase screen spacing

       -- Window navigation
     , ("M-m",           windows W.focusMaster)
     , ("M-j",           windows W.focusDown)
     , ("M-k",           windows W.focusUp)
     , ("M-S-m",         windows W.swapMaster)  -- swap focused & master window
     , ("M-S-j",         windows W.swapDown)    -- swap focused & next window
     , ("M-S-k",         windows W.swapUp)      -- swap focused & prev. window
     , ("M-<Backspace>", promote)               -- moves focused window to master
     , ("M-S-<Tab>",     rotSlavesDown)         -- rotate all slave windows
     , ("M-C-<Tab>",     rotAllDown)            -- rotate all windows

       -- Layouts
     , ("M-<Tab>",   sendMessage NextLayout)                                     -- switch to next layout
     , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- toggles noborder/full

       -- Number of windows
     , ("M-S-<Up>",   sendMessage (IncMasterN 1))    -- increase # of master windows
     , ("M-S-<Down>", sendMessage (IncMasterN (-1))) -- decrease # of master windows
     , ("M-C-<Up>",   increaseLimit)                 -- increase window limit
     , ("M-C-<Down>", decreaseLimit)                 -- decrease window limit
 
       -- Window resizing
     , ("M-h",    sendMessage Shrink)       -- shrink horiz window width
     , ("M-l",    sendMessage Expand)       -- expand horiz window width
     , ("M-M1-j", sendMessage MirrorShrink) -- shrink vert window width
     , ("M-M1-k", sendMessage MirrorExpand) -- expand vert window width

       -- Sublayouts
     , ("M-C-h", sendMessage $ pullGroup L)
     , ("M-C-l", sendMessage $ pullGroup R)
     , ("M-C-k", sendMessage $ pullGroup U)
     , ("M-C-j", sendMessage $ pullGroup D)
     , ("M-C-m", withFocused $ sendMessage . MergeAll)
     , ("M-C-/", withFocused $ sendMessage . UnMergeAll)
     , ("M-C-.", onGroup W.focusUp')
     , ("M-C-,", onGroup W.focusDown')

       -- Multimedia
     , ("<XF86AudioPlay>",          spawn "playerctl play-pause")
     , ("<XF86AudioStop>",          spawn "playerctl stop")
     , ("<XF86AudioPrev>",          spawn "playerctl previous")
     , ("<XF86AudioNext>",          spawn "playerctl next")
     , ("<XF86AudioRaiseVolume>",   spawn "pactl -- set-sink-mute 0 0; pactl -- set-sink-volume 0 +5%") 
     , ("<XF86AudioLowerVolume>",   spawn "pactl -- set-sink-mute 0 0; pactl -- set-sink-volume 0 -5%") 
     , ("<XF86AudioMute>",          spawn "pactl -- set-sink-mute 0 toggle") 
     , ("<XF86MonBrightnessUp>",    spawn "xbacklight -inc 10")
     , ("<XF86MonBrightnessDown>",  spawn "xbacklight -dec 10")
       -- For accidents >:(
     , ("<XF86Sleep>",              spawn "echo foo")
     , ("<XF86Poweroff>",           spawn "echo bar")
    ]

-- Main hook --
main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar -x 0 /home/yasser/.config/xmobar/xmobarrc"
  xmonad $ ewmh $ Hacks.javaHack $ docks $ def
    { manageHook         = myManageHook <+> manageDocks
    --, handleEventHook    = handleEventHook def <+> Hacks.windowedFullscreenFixEventHook <> Hacks.trayerPaddingXmobarEventHook <> Hacks.trayerAboveXmobarEventHook <+> swallowEventHook (className =? "Alacritty" <||> className =? "xterm") (return True)
    , handleEventHook    = handleEventHook def <+> Hacks.windowedFullscreenFixEventHook <> Hacks.trayerPaddingXmobarEventHook <> Hacks.trayerAboveXmobarEventHook
    , modMask            = myModMask
    , terminal           = myTerminal
    , startupHook        = myStartupHook
    , layoutHook         = showWName' myShowWNameTheme $ myLayout
    , workspaces         = myWorkspaces
    , borderWidth        = myBorderWidth
    , normalBorderColor  = myNormColor
    , focusedBorderColor = myFocusColor
    , logHook = dynamicLogWithPP $ xmobarPP
      { ppOutput = hPutStrLn xmproc
      , ppCurrent = xmobarColor "#7da6ff" "" . wrap ("<box type=Bottom width=2 color=#7da6ff>[") "]</box>"
      , ppHidden = xmobarColor "#9ece6a" "" . wrap ("<box type=Bottom width=2 color=#9ece6a>") "</box>"
      , ppHiddenNoWindows = xmobarColor "#a9b1d6" ""
      , ppUrgent = xmobarColor "#ff9e64" "" . wrap ("<box type=Bottom width=2 color=#ff9e64>") "!</box>"
      , ppSep = "<fc=#444b6a> | </fc>"
      , ppWsSep = "<fc=#444b6a>, </fc>"
      , ppTitle = xmobarColor "#bb9af7" "" . shorten 90
      , ppLayout = xmobarColor "#f7768e" ""
      , ppExtras = [windowCount]
      , ppOrder = \(ws:l:t:ex) -> ex++[ws,l,t]
      }
    } `additionalKeysP` myKeys
