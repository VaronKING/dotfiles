# Fish Config 
# -----------

# PATHS
# -----
set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $HOME/.local/bin/dmenu-scripts $HOME/.emacs.d/bin /var/lib/flatpak/exports/bin $fish_user_paths
set -Ux QT_QPA_PLATFORMTHEME qt5ct
set -Ux QT_X11_NO_MITSHM 1

# EXTRA
# -----
set fish_greeting		# Disables fish greeting
set TERM "xterm-256color"	# Sets the terminal
set EDITOR "nvim"		# Terminal editor
set VISUAL "nvim"		# GUI editor

# MANPAGER
# --------
# Uses less
set -x MANPAGER "less"
# Uses bat
# set -x MANPAGER "sh -c 'col -bx | bat-l man -p'"
# Uses vim
# set -x MANPAGER '/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'
# Uses neovim
# set -x MANPAGER "nvim -c 'set ft=man' -"

# KEYBINDINGS
# -----------
function fish_user_key_bindings
  #fish_default_key_bindings
  fish_vi_key_bindings
end

# SPARK
# -----
set -g spark_version 1.0.0

complete -xc spark -n __fish_use_subcommand -a --help -d "Show usage help"
complete -xc spark -n __fish_use_subcommand -a --version -d "$spark_version"
complete -xc spark -n __fish_use_subcommand -a --min -d "Minimum range value"
complete -xc spark -n __fish_use_subcommand -a --max -d "Maximum range value"

function spark -d "sparkline generator"
    if isatty
        switch "$argv"
            case {,-}-v{ersion,}
                echo "spark version $spark_version"
            case {,-}-h{elp,}
                echo "usage: spark [--min=<n> --max=<n>] <numbers...>  Draw sparklines"
                echo "examples:"
                echo "       spark 1 2 3 4"
                echo "       seq 100 | sort -R | spark"
                echo "       awk \\\$0=length spark.fish | spark"
            case \*
                echo $argv | spark $argv
        end
        return
    end

    command awk -v FS="[[:space:],]*" -v argv="$argv" '
        BEGIN {
            min = match(argv, /--min=[0-9]+/) ? substr(argv, RSTART + 6, RLENGTH - 6) + 0 : ""
            max = match(argv, /--max=[0-9]+/) ? substr(argv, RSTART + 6, RLENGTH - 6) + 0 : ""
        }
        {
            for (i = j = 1; i <= NF; i++) {
                if ($i ~ /^--/) continue
                if ($i !~ /^-?[0-9]/) data[count + j++] = ""
                else {
                    v = data[count + j++] = int($i)
                    if (max == "" && min == "") max = min = v
                    if (max < v) max = v
                    if (min > v ) min = v
                }
            }
            count += j - 1
        }
        END {
            n = split(min == max && max ? "▅ ▅" : "▁ ▂ ▃ ▄ ▅ ▆ ▇ █", blocks, " ")
            scale = (scale = int(256 * (max - min) / (n - 1))) ? scale : 1
            for (i = 1; i <= count; i++)
                out = out (data[i] == "" ? " " : blocks[idx = int(256 * (data[i] - min) / scale) + 1])
            print out
        }
    '
end

# FUNCTIONS
# ---------

# Spark functions
function letters
    cat $argv | awk -vFS='' '{for(i=1;i<=NF;i++){ if($i~/[a-zA-Z]/) { w[tolower($i)]++} } }END{for(i in w) print i,w[i]}' | sort | cut -c 3- | spark | lolcat
    printf  '%s\n' 'abcdefghijklmnopqrstuvwxyz'  ' ' | lolcat
end

function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

# The bindings for !! and !$
if [ $fish_key_bindings = "fish_vi_key_bindings" ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

# Function for creating a backup file
# ex: backup file.txt
# result: copies file as file.txt.bak
function backup --argument filename
    cp $filename $filename.bak
end

# Function for copying files and directories, even recursively.
# ex: copy DIRNAME LOCATIONS
# result: copies the directory and all of its contents.
function copy
    set count (count $argv | tr -d \n)
    if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
        command cp -r $from $to
    else
        command cp $argv
    end
end

# Function for printing a column (splits input on whitespace)
# ex: echo 1 2 3 | coln 3
# output: 3
function coln
    while read -l input
        echo $input | awk '{print $'$argv[1]'}'
    end
end

# Function for printing a row
# ex: seq 3 | rown 3
# output: 3
function rown --argument index
    sed -n "$index p"
end

# Function for ignoring the first 'n' lines
# ex: seq 10 | skip 5
# results: prints everything but the first 5 lines
function skip --argument n
    tail +(math 1 + $n)
end

# Function for taking the first 'n' lines
# ex: seq 10 | take 5
# results: prints only the first 5 lines
function take --argument number
    head -$number
end

# AUTOCOMPLETE AND HIGHLIGHT COLORS
# ---------------------------------
set fish_color_normal brgreen
set fish_color_autosuggestion '#9c8089'
set fish_color_command brgreen
set fish_color_error '#ff6c6b'
set fish_color_param brgreen

# ALIASES
# -------

# Navigation
alias ..="cd .."
alias ...="cd ../.."
alias .3="cd ../../.."
alias .4="cd ../../../.."
alias .5="cd ../../../../.."

# ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias psmem="ps auxf | sort -nr -k 4"
alias pscpu="ps auxf | sort -nr -k 3"

# Merge Xresources
alias Xmerge="xrdb -merge ~/.Xresources"

# ls
alias ls="exa -al --color=always --icons --group-directories-first" # regular listing
alias la="exa -a --color=always --icons --group-directories-first"  # all files and dirs
alias ll="exa -l --color=always --icons --group-directories-first"  # long format
alias lt="exa -aT --color=always --group-directories-first" # tree listing
alias l.="exa -a | egrep "^\.""

# grep
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"

# Updating & Mirrors
 # Updating (Pacman & Paru)
alias pacup="sudo pacman -Syu" # Updates repos and offical packages w/ pacman
alias aurup="paru -Sua --noconfirm" # Updates AUR packages w/ paru
alias allup="paru -Syu --noconfirm" # Updates all repos and packages w/ paru
alias pacl="pacman -Qe" # Lists all installed offical packages
alias paclc="pacman -Qe | wc -l" # Displays the number of installed offical packages
alias pacls="pacman -Qe | grep" # Searches installed packages
alias aurl="pacman -Qm" # Lists all installed AUR packages
alias aurlc="pacman -Qm | wc -l" # Displays the number of installed AUR packages
alias aurls="pacman -Qm | grep" # Searches installed AUR packages
alias pacunl="sudo rm /var/lib/pacman/db.lck" # Removes pacman lock
alias pacclean="sudo pacman -Rns (pacman -Qtdg)" # Removes orphaned packages
 # Mirrors
alias ref="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias refd="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias refs="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias refa="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# git
alias gitb="/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME"

# Other
# alias clear="echo -en "\x1b[2J\x1b[1;1H" ; echo; echo; seq 1 (tput cols) | sort -R | spark | lolcat; echo; echo"
alias jctl="journalctl -p 3 -xb"
alias mkdir="mkdir -pv"
alias systemctl="sudo systemctl"
alias rr="curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash"
alias vim="nvim"
alias emacs="emacsclient -c -a "emacs""
alias shutdown="shutdown now"
 # Confirmation when using cp,mv & rm
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
 # Flags for df & free
alias df="df -h"
alias free="free -m"
 # gpg
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# STARSHIP
# --------
starship init fish | source
