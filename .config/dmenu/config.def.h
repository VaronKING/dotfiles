/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"FiraCode Nerd Font:size=12:antialias=true:hinting=true",
	"FiraCode Nerd Font:size=12"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][10] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#a9b1d6", "#1a1b26", "#24283b" },
	[SchemeSel] = { "#1a1b26", "#7da6ff", "#7aa2f7" },
	[SchemeOut] = { "#1a1b26", "#bb9af7", "#ad8ee6" },
    [SchemeHover] = {"#7da6ff", "#444b6a", "#32344a"},
    [SchemeGreen] = {"#1a1b26", "#b9f27c", "#9ece6a"},
    [SchemeRed] = {"#1a1b26", "#ff7a93", "#f7768e"},
    [SchemeYellow] = {"#1a1b26", "#ff9e64", "#e0af68"},
    [SchemeBlue] = {"#1a1b26", "#7aa2f7", "#7da6ff"},
    [SchemePurple] = {"#1a1b26", "#ad8ee6", "#bb9af7"},
	[SchemeSelHighlight] = { "#1a1b26", "#b9f27c", "#9ece6a" },
	[SchemeNormHighlight] = { "#1a1b26", "#e0af68", "#ff9e64" },
	[SchemeMid] = { "#7da6ff", "#32344a", "#444b6a" },
};
/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 0;
static unsigned int columns    = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0; /* -h option; minimum height of a menu line */
static unsigned int min_lineheight = 8;
static int sely = 0;
static int commented = 0;
static int animated = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
