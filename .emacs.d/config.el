(defvar elpaca-installer-version 0.5)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
			:ref nil
			:files (:defaults (:exclude "extensions"))
			:build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
 (build (expand-file-name "elpaca/" elpaca-builds-directory))
 (order (cdr elpaca-order))
 (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
  (if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
	   ((zerop (call-process "git" nil buffer t "clone"
				 (plist-get order :repo) repo)))
	   ((zerop (call-process "git" nil buffer t "checkout"
				 (or (plist-get order :ref) "--"))))
	   (emacs (concat invocation-directory invocation-name))
	   ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
				 "--eval" "(byte-recompile-directory \".\" 0 'force)")))
	   ((require 'elpaca))
	   ((elpaca-generate-autoloads "elpaca" repo)))
      (progn (message "%s" (buffer-string)) (kill-buffer buffer))
    (error "%s" (with-current-buffer buffer (buffer-string))))
((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)

(elpaca `(,@elpaca-order))

;; Install use-package support
(elpaca elpaca-use-package
  ;; Enable :elpaca use-package keyword.
  (elpaca-use-package-mode)
  ;; Assume :elpaca t unless otherwise specified.
  (setq elpaca-use-package-by-default t))

;; Block until current queue processed.
(elpaca-wait)

(use-package evil
  :init
  (setq evil-want-keybinding nil
        evil-want-C-i-jump nil ;; make tab work like intended in org-mode
        evil-undo-system 'undo-redo
        evil-search-module 'evil-search
        evil-vsplit-window-right t
        evil-split-window-below t)
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package general
  :config
  (general-evil-setup)

  ;; Setting "SPC" as the leader key
  (general-create-definer leader-key
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC"
    :global-prefix "M-SPC")

(leader-key
  "b" '(:ignore t :wk "Buffer")
  "bb" '(switch-to-buffer :wk "Switch buffer")
  "bi" '(ibuffer :wk "Ibuffer")
  "bk" '(kill-this-buffer :wk "Kill buffer")
  "bn" '(next-buffer :wk "Next buffer")
  "bp" '(previous-buffer :wk "Previous buffer")
  "br" '(revert-buffer :wk "Reload/revert buffer"))

(leader-key
  "h" '(:ignore t :wk "Help")
  "ha" '(emacs-version  :wk "Display current Emacs version")
  "hA" '(about-emacs :wk "About Emacs")

  "hb" '(describe-bindings :wk "Describe bindings")
  "hc" '(describe-char :wk "Describe character")
  "hf" '(describe-function :wk "Describe function")
  "hk" '(describe-key :wk "Describe key")
  "hv" '(describe-variable :wk "Describe variable")
  
  "hm" '(:ignore t :wk "Manual")
  "hme" '(info-emacs-manual :wk "Emacs manual")
  "hmi" '(info-display-manual :wk "Open a manual")
  "hmm" '(manual-entry :wk "Open a man page")
  "hmo" '(org-info :wk "Org Mode manual"))

(leader-key
  "e" '(:ignore t :wk "Evaluate elisp")
  "eb" '(eval-buffer :wk "Evaluate buffer")
  "ed" '(eval-defun :wk "Evaluate function")
  "ee" '(eval-expression :wk "Evaluate expression")
  "el" '(eval-last-sexp :wk "Evaluate last expression")
  "er" '(eval-region :wk "Evaluate region"))

;; `:states 'normal` is necessary here bind "C-o" to jump-forward
;; instead of backward, because "C-i" functions like "TAB".
;; This makes `general-define-key` act like `evil-define-key`.
(general-define-key :states 'normal
		    "C-o" 'evil-jump-forward
		    "C-S-o" 'evil-jump-backward)

(leader-key
  "." '(find-file :wk "Find file")

  ;; Files
  "f" '(:ignore t :wk "File")
  "fc" '(load-user-config-file :wk "Open Emacs config")
  "fr" '(counsel-recentf :wk "Show recent files")
  "fs" '(save-buffer :wk "Save buffer/file")

  ;; sudo-edit
  "fS" '(:ignore t :wk "Sudo")
  "fS." '(sudo-edit-find-file :wk "Open a file as sudo")
  "fSe" '(sudo-edit :wk "Open this file as sudo"))

(leader-key :keymaps 'org-mode-map
  "m" '(:ignore t :wk "Org Mode")
  "me" '(org-export-dispatch :wk "Org Export Dispatcher"))

(general-define-key :keymaps 'org-mode-map
		    "C-<return>" 'org-meta-return
		    :states 'normal
		    "RET" 'org-open-at-point)

(leader-key
  "s" '(:ignore t :wk "Shell")
  "se" '(eshell :wk "Eshell")
  "sh" '(counsel-esh-history :wk "Eshell history"))
  ;;"sv" '(vterm :wk "Vterm")

(leader-key
  "q" '(:ignore t :wk "Session")
  ;;"ql" '( "~/.emacs.d/sessions/" :wk "Load session")
  ;; This doesn't necessarily reload all changes made to the config,
  ;; often times it's better to evaluate new code or restart Emacs.
  "qr" '(load-user-init-file :wk "Reload init file")
  ;;"qs" '( :wk "Save session")
  "qN" '(restart-emacs-start-new-emacs :wk "Start new session")
  "qR" '(restart-emacs :wk "Restart Emacs session"))

(leader-key
  "t" '(:ignore t :wk "Toggle")
  "tl" '(display-line-numbers-mode :wk "Toggle line numbers")
  "tp" '(org-toggle-pretty-entities :wk "Toggle org pretty entities")
  "ts" '(flyspell-mode :wk "Toggle spellcheck")
  "tt" '(toggle-truncate-lines :wk "Toggle truncated lines")
  "tv" '(vterm-toggle :wk "Toggle Vterm"))

(leader-key
  "w" '(:ignore t :wk "Window")

  ;; Splits
  "wc" '(evil-window-delete :wk "Close current window")
  "wn" '(evil-window-new :wk "Open a window")
  "wo" '(delete-other-windows :wk "Close other windows")
  "ws" '(evil-window-split :wk "Split window horizontally")
  "wv" '(evil-window-vsplit :wk "Split window vertically")

  ;; Window movement
  "wh" '(evil-window-left :wk "Move window left")
  "wj" '(evil-window-down :wk "Move window down")
  "wk" '(evil-window-up :wk "Move window up")
  "wl" '(evil-window-right :wk "Move window right")
  "ww" '(evil-window-next :wk "Go to the next window")

  ;; Buffer movement
  "wH" '(buf-move-left :wk "Move buffer left")
  "wJ" '(buf-move-down :wk "Move buffer down")
  "wK" '(buf-move-up :wk "Move buffer up")
  "wL" '(buf-move-right :wk "Move buffer right"))

(leader-key
  "l" '(comment-line :wk "Comment line")
  
  "G" '(:ignore t :wk "Games")
  "Gt" '(tetris :wk "Tetris"))

(general-define-key
 "C-=" 'text-scale-increase
 "C-+" 'text-scale-increase
 "C--" 'text-scale-decrease)

(leader-key
  "=" '(text-scale-adjust :wk "Zoom in")
  "-" '(text-scale-adjust :wk "Zoom out")
  "0" '(text-scale-adjust :wk "Reset zoom"))

(general-define-key [escape] 'keyboard-escape-quit))

(use-package all-the-icons
  :ensure t
  :if (display-graphic-p))

(use-package all-the-icons-dired
  :hook (dired-mode . (lambda () (all-the-icons-dired-mode t))))

(defun load-user-init-file ()
  "Loads the user's init.el file.
Runs (load-file user-init-file).
See `load-file' and `user-init-file'."
  (interactive)
  (load-file user-init-file))

(defvar user-org-config-file (concat user-emacs-directory "config.org")
  "File name and directory of user's config.org file, which is loaded by init.el.
See `user-init-file'.")

(defun load-user-config-file ()
  "Loads the user's config.org file.
Runs (find-file user-org-config-file).
See `find-file' and `user-org-config-file'."
  (interactive)
  (find-file user-org-config-file))

(setq make-backup-files nil)

(set-face-attribute 'default nil
  :font "Hack"
  :height 120
  :weight 'normal)
(set-face-attribute 'variable-pitch nil
  :font "FiraCode Nerd Font"
  :height 100
  :weight 'normal)
(set-face-attribute 'fixed-pitch nil
  :font "Hack"
  :height 120
  :weight 'normal)

(add-to-list 'default-frame-alist '(font . "Hack-12"))

;; (add-hook 'text-mode-hook flyspell-mode)

(use-package counsel
  :after ivy
  :config (counsel-mode))

(use-package ivy
  :custom
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) ")
  (setq enable-recursive-minibuffers t)
  :config
  (ivy-mode))

(use-package all-the-icons-ivy-rich
  :ensure t
  :init (all-the-icons-ivy-rich-mode 1))

(use-package ivy-rich
  :after ivy
  :ensure t
  :init (ivy-rich-mode 1) ;; this gets us descriptions in M-x.
  :custom
  (ivy-virtual-abbreviate 'full
   ivy-rich-switch-buffer-align-virtual-buffer t
   ivy-rich-path-style 'abbrev))

(load-file (concat user-emacs-directory "local-programs/buffer-move.el"))

(setq org-return-follows-link t)

(electric-indent-mode -1)
(setq org-edit-src-content-indentation 0)

(add-hook 'org-mode-hook 'org-indent-mode)
(use-package org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(require 'org-tempo)

(use-package toc-org
  :commands toc-org-enable
  :init (add-hook 'org-mode-hook 'toc-org-enable))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package pdf-tools
  :ensure t
  :mode ("\\.pdf\\'" . pdf-view-mode) ;; use `pdf-view-mode` instead of `doc-view-mode`
  :config
  (pdf-tools-install)
  (setq pdf-view-display-size 'fit-page))

(use-package rainbow-mode
  :init
  (define-globalized-minor-mode global-rainbow-mode rainbow-mode
    (lambda () (rainbow-mode 1)))
  (global-rainbow-mode 1))

(use-package restart-emacs)

(use-package selectric-mode)

(use-package eshell-syntax-highlighting
  :after eshell-mode
  :config
  (eshell-syntax-highlighting-global-mode +1))

(setq eshell-rc-script (concat user-emacs-directory "eshell/profile")
      eshell-aliases-file (concat user-emacs-directory "eshell/aliases")
      eshell-history-size 10000
      eshell-buffer-maximum-lines 10000
      eshell-hist-ignoredups t
      eshell-scroll-to-bottom-on-input t
      eshell-destroy-buffer-when-process-dies t
      eshell-visual-commands '("bash" "fish" "htop" "ssh" "top" "zsh"))

(use-package vterm
  :config
  (setq shell-file-name "/bin/sh"
	vterm-max-scroll-back 10000))

(use-package vterm-toggle
  :after vterm
  :config
  (setq vterm-toggle-fullscreen-p nil)
  (add-to-list 'display-buffer-alist
               '((lambda (buffer-or-name _)
                   (let ((buffer (get-buffer buffer-or-name)))
                     (with-current-buffer buffer
                       (or (equal major-mode 'vterm-mode)
                           (string-prefix-p vterm-buffer-name (buffer-name buffer))))))
                 (display-buffer-reuse-window display-buffer-at-bottom)
                 ;;(display-buffer-reuse-window display-buffer-in-direction)
                 ;;display-buffer-in-direction/direction/dedicated is added in emacs27
                 ;;(direction . bottom)
                 ;;(dedicated . t) ;dedicated is supported in emacs27
                 (reusable-frames . visible)
                 (window-height . 0.3))))

(use-package sudo-edit)

(defadvice tetris-end-game (around zap-scores activate)
  (save-window-excursion ad-do-it))

(add-to-list 'custom-theme-load-path (concat user-emacs-directory "themes/"))
;; (load-theme 'yasser-tokyo-night t)

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-tokyo-night t)

  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

(define-globalized-minor-mode global-window-divider-mode window-divider-mode
 (lambda () (window-divider-mode 1)))

(global-window-divider-mode 1)

(setq window-divider-default-places t
      window-divider-default-right-width 1
      window-divider-default-bottom-width 1)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(global-visual-line-mode)
(global-display-line-numbers-mode) ;; enabling line numbers

;; Disabling line-numbers in major-modes that either don't need it
;; or have incompatiblity problems with it
(dolist (disable-line-numbers-hooks '(pdf-view-mode-hook))
  (add-hook disable-line-numbers-hooks (lambda () (display-line-numbers-mode -1))))

(use-package which-key
  :init
  (setq which-key-idle-delay 0.5)
  (which-key-mode 1)
  :config
  (setq which-key-side-window-location 'top
	which-key-sort-order #'which-key-prefix-then-key-order
	which-key-allow-imprecise-window-fit nil
	which-key-sort-uppercase-first nil
	which-key-add-column-padding 1
	which-key-max-display-columns nil
	which-key-min-display-lines 7
	which-key-side-window-slot -10
	which-key-side-window-max-height 0.35
	which-key-max-description-length 40
	which-key-show-remaining-keys t
	which-key-prefix-prefix "• "
	which-key-separator " │ " ))
