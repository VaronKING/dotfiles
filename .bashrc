# Bash Config
# -----------

# PATHS
# -----

export PATH="$HOME/.local/bin:$HOME/.emacs.d/bin:$HOME/:/var/lib/flatpak/exports/bin:$PATH"

# EXTRA
# -----
export TERM="xterm-256color"                      # getting proper colors
export HISTCONTROL=ignoredups:erasedups           # no duplicate entries
export ALTERNATE_EDITOR=""                        # setting for emacsclient
export EDITOR="emacsclient -t -a ''"              # $EDITOR use Emacs in terminal
export VISUAL="emacsclient -c -a emacs"           # $VISUAL use Emacs in GUI mode

# MANPAGER
# --------
# Uses less
export MANPAGER="less"
# Uses bat
# export MANPAGER="sh -c 'col -bx | bat -l man -p'"
# Uses vim
# export MANPAGER='/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'
# Uses neovim
# export MANPAGER="nvim -c 'set ft=man' -"

# VI MODE
# -------
# Comment this line out to enable default emacs-like bindings
set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# CHANGE TITLE OF TERMINALS
# -------------------------
case ${TERM} in
  xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|alacritty|st|konsole*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
        ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
    ;;
esac

# SHOPT
# -----
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases
shopt -s checkwinsize # checks term size when bash regains control

# CASE INSENSITIVTY DURING TAB COMPLETION
# ---------------------------------------
bind "set completion-ignore-case on"

# ARCHIVE EXTRACTION
# ------------------
 # usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# ALIASES
# -------

# Navigation
up () {
  local d=""
  local limit="$1"

  # Default to limit of 1
  if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
    limit=1
  fi

  for ((i=1;i<=limit;i++)); do
    d="../$d"
  done

  # Perform cd. Show error if cd fails
  if ! cd "$d"; then
    echo "Couldn't go up $limit dirs.";
  fi
}

# ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias psmem='ps auxf | sort -nr -k 4'
alias pscpu='ps auxf | sort -nr -k 3'

# Merge Xresources
alias Xmerge='xrdb -merge ~/.Xresources'

# ls
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# grep
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Updating & Mirrors
 # Updating (Pacman & Paru)
alias pacup='sudo pacman -Syu' # Updates repos and offical packages w/ pacman
alias aurup='paru -Sua --noconfirm' # Updates AUR packages w/ paru
alias allup='paru -Syu --noconfirm' # Updates all repos and packages w/ paru
alias pacl='pacman -Qe' # Lists all installed offical packages
alias paclc='pacman -Qe | wc -l' # Displays the number of installed offical packages
alias pacls='pacman -Qe | grep' # Searches installed packages
alias aurl='pacman -Qm' # Lists all installed AUR packages
alias aurlc='pacman -Qm | wc -l' # Displays the number of installed AUR packages
alias aurls='pacman -Qm | grep' # Searches installed AUR packages
alias pacunl='sudo rm /var/lib/pacman/db.lck' # Removes pacman lock
alias pacclean='sudo pacman -Rns (pacman -Qtdg)' # Removes orphaned packages
 # Mirrors
alias ref="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias refd="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias refs="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias refa="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# git
alias gitb='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME'

# Other
alias jctl="journalctl -p 3 -xb"
alias mkdir='mkdir -pv'
alias systemctl='sudo systemctl'
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'
alias vim='nvim'
alias emacs='emacsclient -c -a 'emacs''
alias shutdown='shutdown now'
# Confirmation when using cp,mv & rm
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'
 # Flags for df & free
alias df='df -h'
alias free='free -m'
 # gpg
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# STARSHIP
# --------
eval "$(starship init bash)"
